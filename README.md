# Cache

Middleware cache provides cache management for [Tango](https://gitea.com/lunny/tango). It can use many cache adapters, including memory, file, Redis, Memcache, PostgreSQL, MySQL, Ledis and Nodb.

## Installation

	go get gitea.com/tango/cache

## Getting Help

- [API Reference](https://gowalker.org/gitea.com/tango/cache)

## More adapters

Default cache includes two adapters memory and file, you can find more adapters.

- [Redis](http://gitea.com/tango/cache-redis)
- [Memcache](http://gitea.com/tango/cache-memcache)
- [PostgreSQL](http://gitea.com/tango/cache-postgres)
- [MySQL](http://gitea.com/tango/cache-mysql)
- [Ledis](http://gitea.com/tango/cache-ledis)
- [Nodb](http://gitea.com/tango/cache-nodb)

## Credits

This package is a modified version of [macaron/cache](https://github.com/go-macaron/cache).

## License

This project is under the Apache License, Version 2.0. See the [LICENSE](LICENSE) file for the full license text.